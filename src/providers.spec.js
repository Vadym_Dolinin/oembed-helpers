const fs = require('fs');
const path = require('path');
const expect = require('chai').expect;

const detect = require('./');
const Providers = require('./Provider').Providers;

const providersList = fs.readdirSync(path.join(__dirname, './Provider'))
  .map((f) => path.join(__dirname, 'Provider', f))
  .filter((f) => fs.statSync(f).isDirectory())
  .map((f) => require(f))
  .map((provider) => {
    const key = Object.keys(provider)[0];
    return provider[key].provider;
  })
  ;

const providesNames = Providers.map((provider) => provider.provider);

describe(`Massive detection function`, function () {
  it(`Should return null if URL not passed`, () => {
    expect(detect()).equal(null);
  });

  it(`Providers should contain all enabled providers`, () => {
    providersList.map((name) => expect(providesNames).to.include(name, name));
  });

  it(`Providers should be a valid array`, () => {
    Providers.map((provider) => {
      expect(provider).to.be.an('object').that.has.all.keys(['provider', 'detect', 'getUrl']);
      expect(provider.provider).to.be.an('string');
      expect(provider.detect).to.be.an('function');
      expect(provider.getUrl).to.be.an('function');
    });
  });
});
