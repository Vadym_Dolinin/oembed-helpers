module.exports.SoundCloud = {
  provider: 'SoundCloud',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
