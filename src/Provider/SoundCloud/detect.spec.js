const expect = require('chai').expect;

const detect = require('./detect');

describe('SoundCloud', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "http://www.soundcloud.com/"`, () => {
    expect(detect('http://www.soundcloud.com/')).to.be.false;
  });

  it(`Should NOT detect "http://soundcloud.com/forss/"`, () => {
    expect(detect('http://soundcloud.com/forss/')).to.be.false;
  });

  it(`Should NOT detect "http://soundcloud.com/forss/sets/"`, () => {
    expect(detect('http://soundcloud.com/forss/sets/')).to.be.false;
  });

  it(`Should detect "http://soundcloud.com/groups/voidavail"`, () => {
    expect(detect('http://soundcloud.com/groups/voidavail')).to.be.true;
  });

  it(`Should detect "http://soundcloud.com/forss/flickermood"`, () => {
    expect(detect('http://soundcloud.com/forss/flickermood')).to.be.true;
  });

  it(`Should detect "http://soundcloud.com/forss/sets/flickermood"`, () => {
    expect(detect('http://soundcloud.com/forss/sets/flickermood')).to.be.true;
  });
});
