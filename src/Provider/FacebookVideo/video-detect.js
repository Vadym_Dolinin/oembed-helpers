module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    url.match(new RegExp('https://www\.facebook\.com/.+/videos/.+', 'i'))
    ||
    url.match(new RegExp('https://www\.facebook\.com/video\.php\\?id=.+', 'i'))
    ||
    url.match(new RegExp('https://www\.facebook\.com/video\.php\\?v=.+', 'i'))
  );

}
