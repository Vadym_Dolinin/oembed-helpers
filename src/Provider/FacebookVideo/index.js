module.exports.FacebookVideo = {
  provider: 'Facebook Video',
  detect: require('./video-detect'),
  getUrl: require('./getUrl').videoURL
}
