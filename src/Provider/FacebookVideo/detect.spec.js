const expect = require('chai').expect;

const videoDetect = require('./video-detect');
const postDetect = require('../FacebookPublication/post-detect');

describe('Facebook', function() {
  it(`Should work without passing URL`, () => {
    expect(postDetect()).to.be.false;
    expect(videoDetect()).to.be.false;
  });

  it(`Should NOT detect "http://www.facebook.com/"`, () => {
    expect(postDetect('http://www.facebook.com/')).to.be.false;
    expect(videoDetect('http://www.facebook.com/')).to.be.false;
  });

  describe(`Video`, function() {
    it(`Should NOT detect "https://www.facebook.com/facebook/"`, () => {
      expect(videoDetect('https://www.facebook.com/facebook/videos')).to.be.false;
    });

    it(`Should NOT detect "https://www.facebook.com/facebook/videos/"`, () => {
      expect(videoDetect('https://www.facebook.com/facebook/videos/')).to.be.false;
    });

    it(`Should detect "https://www.facebook.com/facebook/videos/10153231379946729/"`, () => {
      expect(videoDetect('https://www.facebook.com/facebook/videos/10153231379946729/')).to.be.true;
    });

    it(`Should detect "https://www.facebook.com/video.php?id=10153231379946729"`, () => {
      expect(videoDetect('https://www.facebook.com/video.php?id=10153231379946729')).to.be.true;
    });

    it(`Should detect "https://www.facebook.com/video.php?v=10153231379946729"`, () => {
      expect(videoDetect('https://www.facebook.com/video.php?v=10153231379946729')).to.be.true;
    });
  });

});
