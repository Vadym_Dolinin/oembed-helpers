module.exports.videoURL = (url) => {
  if (typeof url !== 'string') {
    return null;
  }

  return `https://www.facebook.com/plugins/video/oembed.json/?url=${escape(url)}`;
}
