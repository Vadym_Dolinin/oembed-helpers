const expect = require('chai').expect;

const postDetect = require('./post-detect');
const videoDetect = require('../FacebookVideo/video-detect');

describe('Facebook', function() {
  it(`Should work without passing URL`, () => {
    expect(postDetect()).to.be.false;
    expect(videoDetect()).to.be.false;
  });

  it(`Should NOT detect "http://www.facebook.com/"`, () => {
    expect(postDetect('http://www.facebook.com/')).to.be.false;
    expect(videoDetect('http://www.facebook.com/')).to.be.false;
  });

  // TODO Write more tests for Facebook Post detector
  describe('Post', function() {
    it(`Should detect "https://www.facebook.com/notes/facebook/introducing-new-apps-for-timeline/10150469721182131/"`, () => {
      expect(postDetect('https://www.facebook.com/notes/facebook/introducing-new-apps-for-timeline/10150469721182131/')).to.be.true;
    });
  });
});
