module.exports.publicationURL = (url) => {
  if (typeof url !== 'string') {
    return null;
  }

  return `https://www.facebook.com/plugins/post/oembed.json/?url=${escape(url)}`;
}
