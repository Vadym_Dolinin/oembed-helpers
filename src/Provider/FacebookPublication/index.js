module.exports.FacebookPublication = {
  provider: 'Facebook Publication',
  detect: require('./post-detect'),
  getUrl: require('./getUrl').publicationURL
}
