module.exports.Gfycat = {
  provider: 'Gfycat',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
