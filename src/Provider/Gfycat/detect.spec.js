const expect = require('chai').expect;

const detect = require('./detect');

describe('Gfycat', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "https://gfycat.com/"`, () => {
    expect(detect('https://gfycat.com/')).to.be.false;
  });

  it(`Should NOT detect "https://gfycat.com/@danno"`, () => {
    expect(detect('https://gfycat.com/@danno')).to.be.false;
  });

  it(`Should NOT detect "https://gfycat.com/RichPepperyFerret/"`, () => {
    expect(detect('https://gfycat.com/RichPepperyFerret/')).to.be.false;
  });

  it(`Should detect "https://gfycat.com/RichPepperyFerret"`, () => {
    expect(detect('https://gfycat.com/RichPepperyFerret')).to.be.true;
  });

  describe(`Detail URL`, function() {
    it(`Should NOT detect "https://gfycat.com/en/"`, () => {
      expect(detect('https://gfycat.com/en/')).to.be.false;
    });

    it(`Should NOT detect "https://gfycat.com/en/gifs"`, () => {
      expect(detect('https://gfycat.com/en/gifs')).to.be.false;
    });

    it(`Should NOT detect "https://gfycat.com/en/gifs/detail"`, () => {
      expect(detect('https://gfycat.com/en/gifs/detail')).to.be.false;
    });

    it(`Should detect "https://gfycat.com/en/gifs/detail/RichPepperyFerret"`, () => {
      expect(detect('https://gfycat.com/en/gifs/detail/RichPepperyFerret')).to.be.true;
    });
  });

});
