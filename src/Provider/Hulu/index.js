module.exports.Hulu = {
  provider: 'Hulu',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
