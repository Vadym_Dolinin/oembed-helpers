const expect = require('chai').expect;

const detect = require('./detect');

describe('Hulu', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "http://www.hulu.com/"`, () => {
    expect(detect('http://www.hulu.com/')).to.be.false;
  });

  it(`Should NOT detect "http://www.hulu.com/late-night-with-conan-obrien-wed-may-21-2008"`, () => {
    expect(detect('http://www.hulu.com/late-night-with-conan-obrien-wed-may-21-2008')).to.be.false;
  });

  it(`Should detect "http://www.hulu.com/embed/20807/late-night-with-conan-obrien-wed-may-21-2008"`, () => {
    expect(detect('http://www.hulu.com/embed/20807/late-night-with-conan-obrien-wed-may-21-2008')).to.be.true;
  });

  it(`Should detect "http://www.hulu.com/w/20807/late-night-with-conan-obrien-wed-may-21-2008"`, () => {
    expect(detect('http://www.hulu.com/w/20807/late-night-with-conan-obrien-wed-may-21-2008')).to.be.true;
  });

  it(`Should detect "http://www.hulu.com/watch/20807/late-night-with-conan-obrien-wed-may-21-2008"`, () => {
    expect(detect('http://www.hulu.com/watch/20807/late-night-with-conan-obrien-wed-may-21-2008')).to.be.true;
  });
});
