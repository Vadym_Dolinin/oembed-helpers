module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `http://www.hulu.com/api/oembed.json?url=${escape(url)}`;
}
