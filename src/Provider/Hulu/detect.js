module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    url.match(/http:\/\/(www\.)?hulu\.com\/watch.+/i)
    ||
    url.match(/http:\/\/(www\.)?hulu\.com\/w\/.+/i)
    ||
    url.match(/http:\/\/(www\.)?hulu\.com\/embed\/.+/i)
  );
}
