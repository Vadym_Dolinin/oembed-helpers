module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    url.match(/http(s)?:\/\/(www\.)?giphy\.com\/gifs\/.+/i)
    ||
    url.match(/http(s)?:\/\/(www\.)?gph\.is\/.+/i)
    ||
    url.match(/http(s)?:\/\/(www\.)?media\.giphy\.com\/media\/.+$/i)
  );
}
