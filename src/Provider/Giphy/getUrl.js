module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://giphy.com/services/oembed?url=${escape(url)}`;
}
