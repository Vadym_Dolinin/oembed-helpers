const expect = require('chai').expect;

const detect = require('./detect');

describe('Giphy', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "https://giphy.com/"`, () => {
    expect(detect('https://giphy.com/')).to.be.false;
  });

  it(`Should NOT detect "https://giphy.com/gifs/"`, () => {
    expect(detect('https://giphy.com/gifs/')).to.be.false;
  });

  it(`Should NOT detect "https://media.giphy.com/media/"`, () => {
    expect(detect('https://media.giphy.com/media/')).to.be.false;
  });

  it(`Should detect "https://giphy.com/gifs/writing-taking-notes-note-xTiTnwmI2nlBpITMre"`, () => {
    expect(detect('https://giphy.com/gifs/writing-taking-notes-note-xTiTnwmI2nlBpITMre')).to.be.true;
  });

  it(`Should detect "https://media.giphy.com/media/xTiTnwmI2nlBpITMre/giphy.gif"`, () => {
    expect(detect('https://media.giphy.com/media/xTiTnwmI2nlBpITMre/giphy.gif')).to.be.true;
  });
});
