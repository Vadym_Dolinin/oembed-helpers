const expect = require('chai').expect;

const detect = require('./detect');

describe('Twitter', function () {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "https://twitter.com/"`, () => {
    expect(detect('https://twitter.com/')).to.be.false;
  });

  describe(`Profile`, () => {
    it(`Should detect "https://twitter.com/TwitterDev"`, () => {
      expect(detect('https://twitter.com/TwitterDev')).to.be.true;
    });

    it(`Should NOT detect "https://twitter.com/TwitterDev/"`, () => {
      expect(detect('https://twitter.com/TwitterDev/')).to.be.false;
    });
  });

  describe(`Lists`, () => {
    it(`Should NOT detect "https://twitter.com/TwitterDev/lists"`, () => {
      expect(detect('https://twitter.com/TwitterDev/lists')).to.be.false;
    });

    it(`Should NOT detect "https://twitter.com/TwitterDev/lists/"`, () => {
      expect(detect('https://twitter.com/TwitterDev/lists/')).to.be.false;
    });

    it(`Should NOT detect "https://twitter.com/TwitterDev/lists/national/parks"`, () => {
      expect(detect('https://twitter.com/TwitterDev/lists/national/parks')).to.be.false;
    });

    it(`Should detect "https://twitter.com/TwitterDev/lists/national-parks"`, () => {
      expect(detect('https://twitter.com/TwitterDev/lists/national-parks')).to.be.true;
    });
  });

  describe(`Likes`, () => {
    it(`Should NOT detect "https://twitter.com/TwitterDev/likes/"`, () => {
      expect(detect('https://twitter.com/TwitterDev/likes/')).to.be.false;
    });

    it(`Should NOT detect "https://twitter.com/TwitterDev/likes/sad"`, () => {
      expect(detect('https://twitter.com/TwitterDev/likes/sad')).to.be.false;
    });

    it(`Should detect "https://twitter.com/TwitterDev/likes"`, () => {
      expect(detect('https://twitter.com/TwitterDev/likes')).to.be.true;
    });
  });

  describe(`Timelines`, () => {
    it(`Should NOT detect "https://twitter.com/TwitterDev/timelines/sad"`, () => {
      expect(detect('https://twitter.com/TwitterDev/timelines/sad')).to.be.false;
    });

    it(`Should NOT detect "https://twitter.com/TwitterDev/timelines/539487832448843776/"`, () => {
      expect(detect('https://twitter.com/TwitterDev/timelines/539487832448843776/')).to.be.false;
    });

    it(`Should NOT detect "https://twitter.com/TwitterDev/timelines/539487832448843776/sad"`, () => {
      expect(detect('https://twitter.com/TwitterDev/timelines/539487832448843776/sad')).to.be.false;
    });

    it(`Should detect "https://twitter.com/TwitterDev/timelines/539487832448843776"`, () => {
      expect(detect('https://twitter.com/TwitterDev/timelines/539487832448843776')).to.be.true;
    });
  });

  describe(`Status`, () => {
    it(`Should NOT detect "https://twitter.com/Interior/status/"`, () => {
      expect(detect('https://twitter.com/Interior/status/')).to.be.false;
    });
    it(`Should NOT detect "https://twitter.com/Interior/status/sad"`, () => {
      expect(detect('https://twitter.com/Interior/status/sad')).to.be.false;
    });
    it(`Should detect "https://twitter.com/Interior/status/507185938620219395"`, () => {
      expect(detect('https://twitter.com/Interior/status/507185938620219395')).to.be.true;
    });
    it(`Should detect "https://twitter.com/Interior/status/507185938620219395/"`, () => {
      expect(detect('https://twitter.com/Interior/status/507185938620219395/')).to.be.true;
    });
  });
});
