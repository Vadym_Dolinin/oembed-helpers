module.exports.Twitter = {
  provider: 'Twitter',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
