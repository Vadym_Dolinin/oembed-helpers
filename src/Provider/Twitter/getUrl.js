module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://publish.twitter.com/oembed?url=${escape(url)}&dnt=1`
}
