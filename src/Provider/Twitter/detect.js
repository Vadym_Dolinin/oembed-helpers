module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    (url.match(/https:\/\/twitter\.com\/.+/i) && !url.match(/https:\/\/twitter\.com\/.+\//i))
    ||
    (url.match(/https:\/\/twitter\.com\/.+\/lists\/.+/i) && !url.match(/https:\/\/twitter\.com\/.+\/lists\/.+\//i))
    ||
    url.match(/https:\/\/twitter\.com\/.+\/likes(?!\/)/i)
    ||
    url.match(/https:\/\/twitter\.com\/.+\/status\/[0-9]+/i)
    ||
    (url.match(/https:\/\/twitter\.com\/.+\/timelines\/[0-9]+/i) && !url.match(/https:\/\/twitter\.com\/.+\/timelines\/[0-9]+\//i))
  );
}
