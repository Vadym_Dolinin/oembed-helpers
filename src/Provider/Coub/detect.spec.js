const expect = require('chai').expect;

const detect = require('./detect');

describe('Coub', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "http://coub.com/"`, () => {
    expect(detect('http://coub.com/')).to.be.false;
  });

  it(`Should NOT detect "http://coub.com/view/"`, () => {
    expect(detect('http://coub.com/view/')).to.be.false;
  });

  it(`Should NOT detect "http://coub.com/embed/"`, () => {
    expect(detect('http://coub.com/embed/')).to.be.false;
  });

  it(`Should detect "http://coub.com/view/fnmw9"`, () => {
    expect(detect('http://coub.com/view/fnmw9')).to.be.true;
  });

  it(`Should detect "http://coub.com/embed/fnmw9"`, () => {
    expect(detect('http://coub.com/embed/fnmw9')).to.be.true;
  });
});
