module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `http://coub.com/api/oembed.json?url=${escape(url)}`
}
