const expect = require('chai').expect;

const detect = require('./detect');

describe('Instagram', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "http://instagram.com/"`, () => {
    expect(detect('http://instagram.com/')).to.be.false;
  });

  it(`Should NOT detect "http://instagram.com/p/"`, () => {
    expect(detect('http://instagram.com/p/')).to.be.false;
  });

  it(`Should NOT detect "http://instagr.am/p/"`, () => {
    expect(detect('http://instagr.am/p/')).to.be.false;
  });

  it(`Should detect "http://instagram.com/p/V8UMy0LjpX/"`, () => {
    expect(detect('http://instagram.com/p/V8UMy0LjpX/')).to.be.true;
  });

  it(`Should detect "http://instagr.am/p/V8UMy0LjpX/"`, () => {
    expect(detect('http://instagram.com/p/V8UMy0LjpX/')).to.be.true;
  });
});
