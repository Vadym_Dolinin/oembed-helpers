module.exports.Instagram = {
  provider: 'Instagram',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
