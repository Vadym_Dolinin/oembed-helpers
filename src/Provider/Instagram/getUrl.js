module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://api.instagram.com/oembed/?url=${escape(url)}`;
}
