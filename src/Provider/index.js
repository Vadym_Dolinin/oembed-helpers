const { CodePen } = require('./CodePen');
const { Coub } = require('./Coub');
const { Dailymotion } = require('./Dailymotion');
const { DeviantArt } = require('./DeviantArt');
const { FacebookPublication } = require('./FacebookPublication');
const { FacebookVideo } = require('./FacebookVideo');
const { Flickr } = require('./Flickr');
const { Gfycat } = require('./Gfycat');
const { Giphy } = require('./Giphy');
const { Gyazo } = require('./Gyazo');
const { Hulu } = require('./Hulu');
const { Instagram } = require('./Instagram');
const { Kickstarter } = require('./Kickstarter');
const { SlideShare } = require('./SlideShare');
const { SoundCloud } = require('./SoundCloud');
const { Sway } = require('./Sway');
const { Twitter } = require('./Twitter');
const { Vimeo } = require('./Vimeo');
const { YouTube } = require('./YouTube');

module.exports.Providers = [
  CodePen,
  Coub,
  Dailymotion,
  DeviantArt,
  Flickr,
  FacebookPublication,
  FacebookVideo,
  Gfycat,
  Giphy,
  Gyazo,
  Hulu,
  Instagram,
  Kickstarter,
  SlideShare,
  SoundCloud,
  Sway,
  Twitter,
  Vimeo,
  YouTube
];
