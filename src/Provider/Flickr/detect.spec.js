const expect = require('chai').expect;

const detect = require('./detect');

describe('Flickr', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "https://www.flickr.com/"`, () => {
    expect(detect('https://www.flickr.com/')).to.be.false;
  });

  it(`Should NOT detect "https://www.flickr.com/photos/"`, () => {
    expect(detect('https://www.flickr.com/photos/')).to.be.false;
  });

  it(`Should NOT detect "https://www.flickr.com/photos/bees/"`, () => {
    expect(detect('https://www.flickr.com/photos/bees/')).to.be.false;
  });

  it(`Should detect "https://www.flickr.com/photos/bees/2341623661/"`, () => {
    expect(detect('https://www.flickr.com/photos/bees/2341623661/')).to.be.true;
  });
});
