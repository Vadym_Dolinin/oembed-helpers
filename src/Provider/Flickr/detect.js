module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    url.match(/http(s)?:\/\/(www\.)?flickr\.com\/photos\/.+\/[0-9]+/i)
    ||
    url.match(/http(s)?:\/\/flic\.kr\/.+/i)
  );
}
