module.exports.YouTube = {
  provider: 'YouTube',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
