module.exports = (url) => {
  if(typeof url !== 'string') {
    return false;
  }

  if (
    url.match(new RegExp('https://(www\.)?youtube\.com/watch', 'i'))
    ||
    url.match(new RegExp('http(s)?://youtu\.be/.+', 'i'))
  ) {
    return true;
  }

  return false;
}
