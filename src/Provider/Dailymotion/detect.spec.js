const expect = require('chai').expect;

const detect = require('./detect');

describe('Dailymotion', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "http://www.dailymotion.com/"`, () => {
    expect(detect('http://www.dailymotion.com/')).to.be.false;
  });

  it(`Should NOT detect "http://www.dailymotion.com/video/"`, () => {
    expect(detect('http://www.dailymotion.com/video/')).to.be.false;
  });

  it(`Should detect "http://www.dailymotion.com/video/x5nj9nw"`, () => {
    expect(detect('http://www.dailymotion.com/video/x5nj9nw')).to.be.true;
  });
});
