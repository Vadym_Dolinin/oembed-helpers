module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!url.match(/http(s)?:\/\/.*dailymotion\.com\/.*video\/.+/i);
}
