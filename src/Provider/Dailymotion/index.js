module.exports.Dailymotion = {
  provider: 'Dailymotion',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
