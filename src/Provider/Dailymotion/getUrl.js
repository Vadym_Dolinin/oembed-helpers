module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `http://www.dailymotion.com/services/oembed?format=json&url=${escape(url)}`
}
