module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://api.gyazo.com/api/oembed?url=${escape(url)}`;
}
