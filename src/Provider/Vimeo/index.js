module.exports.Vimeo = {
  provider: 'Vimeo',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
