const expect = require('chai').expect;

const detect = require('./detect');

describe('Vimeo', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "https://vimeo.com/"`, () => {
    expect(detect('https://vimeo.com/')).to.be.false;
  });

  it(`Should NOT detect "https://vimeo.com/post"`, () => {
    expect(detect('https://vimeo.com/post')).to.be.false;
  });

  it(`Should detect "https://vimeo.com/18842873"`, () => {
    expect(detect('https://vimeo.com/18842873')).to.be.true;
  });
});
