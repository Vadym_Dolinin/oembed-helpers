module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    url.match(/http(s)?:\/\/(www\.)?vimeo\.com\/[0-9]+/i)
    ||
    url.match(/http(s)?:\/\/(www\.)?vimeo\.com\/groups\/.+\/videos\/.+/i)
    ||
    url.match(/http(s)?:\/\/vimeo\.com\/m\/\#\/.+/i)
    ||
    url.match(/http(s)?:\/\/player\.vimeo\.com\/[0-9]+/i)
  )
}
