const expect = require('chai').expect;

const detect = require('./detect');

describe('Kickstarter', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "http://www.kickstarter.com/"`, () => {
    expect(detect('http://www.kickstarter.com/')).to.be.false;
  });

  it(`Should NOT detect "http://www.kickstarter.com/projects/"`, () => {
    expect(detect('http://www.kickstarter.com/projects/')).to.be.false;
  });

  it(`Should NOT detect "http://www.kickstarter.com/projects/1222"`, () => {
    expect(detect('http://www.kickstarter.com/projects/1222')).to.be.false;
  });

  it(`Should detect "https://www.kickstarter.com/projects/1523379957/oculus-rift-step-into-the-game?ref=discovery&term=oculus%20rift"`, () => {
    expect(detect('https://www.kickstarter.com/projects/1523379957/oculus-rift-step-into-the-game?ref=discovery&term=oculus%20rift')).to.be.true;
  });
});
