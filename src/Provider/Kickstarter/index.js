module.exports.Kickstarter = {
  provider: 'Kickstarter',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
