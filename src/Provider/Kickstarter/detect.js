module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!url.match(/http(s)?:\/\/(www\.)?kickstarter\.com\/projects\/[0-9]+\/.+/i);
}
