module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    url.match(/http(s)?:\/\/(.*)slideshare\.net\/mobile\/.+\/.+/i)
    ||
    url.match(/http(s)?:\/\/(.*)slideshare\.net\/(?!mobile\/).+\/.+/i)
    ||
    url.match(/http(s)?:\/\/slidesha\.re\/.+/i)
  );
}
