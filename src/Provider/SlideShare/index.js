module.exports.SlideShare = {
  provider: 'SlideShare',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
