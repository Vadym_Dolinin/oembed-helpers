module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    url.match(/https:\/\/.+\.deviantart\.com\/art\/.+/i)
    ||
    url.match(/http:\/\/fav\.me\/.+/i)
    ||
    url.match(/https:\/\/sta\.sh\/.+/i)
    ||
    url.match(/https:\/\/.+\.deviantart\.com\/.+\#\/d.+/i)
  );
}
