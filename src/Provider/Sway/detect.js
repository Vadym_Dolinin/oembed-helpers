module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!url.match(/https:\/\/sway\.com\/.+/i);
}
