module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://sway.com/api/v1.0/oembed?format=json&url=${escape(url)}`;
}
